const toFullId = (id) => `vcs.rules.${id}`;
export const makeResult = ({
  severity = 'WARNING',
  id = 'bad-button',
  path = 'some/path.vue',
} = {}) => ({
  check_id: toFullId(id),
  end: {
    col: 22,
    line: 46,
    offset: 2542,
  },
  extra: {
    lines: 'some code',
    message: 'some message',
    severity,
  },
  path,
  start: {
    col: 14,
    line: 46,
    offset: 2534,
  },
});

const makeRule = ({
  id,
  severity = 'WARNING',
  message = `message for ${id}`,
  componentLabel = 'component:button',
  pajamasCompliant = false,
} = {}) => ({
  id: toFullId(id),
  message,
  severity,
  metadata: {
    componentLabel,
    pajamasCompliant,
  },
});

export const invalidScans = [undefined, null, {}, { results: [{}] }];

export const getEmptyScan = () => ({ results: [] });

export const getMinimalScan = () => ({ results: [makeResult()] });

export const getTypicalScan = () => ({
  results: [
    {},
    { path: 'different/file_type.js' },
    { id: 'okay-button', severity: 'INFO', pajamasCompliant: true },
    { id: 'bad-alert', componentLabel: 'component:alert' },
  ].map(makeResult),
});

export const getHistory = () => [
  {
    date: '2021-01-01',
    summary: {
      'bad-alert': 1,
      'bad-button': 1,
      'okay-button': 1,
    },
  },
  {
    date: '2022-02-02',
    summary: {
      'bad-alert': 2,
      'bad-button': 2,
      'okay-button': 1,
      'okay-label': 2,
    },
  },
];

export const getScanSummary = () => ({
  current: {
    file: 'current_2022-10-27_abc123.json',
  },
  history: getHistory(),
});

export const getRules = () =>
  [
    { id: 'bad-button' },
    { id: 'okay-button', pajamasCompliant: true, severity: 'INFO' },
    { id: 'bad-alert', componentLabel: 'component:alert' },
    {
      id: 'okay-alert',
      componentLabel: 'component:alert',
      pajamasCompliant: true,
      severity: 'INFO',
    },
    { id: 'bad-label', componentLabel: 'component:label' },
    {
      id: 'okay-label',
      componentLabel: 'component:label',
      pajamasCompliant: true,
      severity: 'INFO',
    },
  ].reduce((acc, rule) => {
    acc.set(rule.id, makeRule(rule));
    return acc;
  }, new Map());
