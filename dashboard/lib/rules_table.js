const getFindingsByRuleIdMap = (scan) =>
  scan.results.reduce((acc, finding) => {
    if (!acc.has(finding.check_id)) acc.set(finding.check_id, 0);
    acc.set(finding.check_id, acc.get(finding.check_id) + 1);
    return acc;
  }, new Map());

/**
 * Return the rules-by-id table data props (fields, items, primaryKey) for the
 * given scan and rules
 *
 * @param {Object} The JSON output of a semgrep scan.
 * @param {Promise<Map<string, Rule>>} A map of rule ids to rule definitions.
 * @returns {Object} The fields, items and primaryKey props for a table.
 */
export const allRulesTableDataProps = (scan, rules) => {
  const fields = [
    { key: 'id', label: 'Rule ID', sortable: false },
    { key: 'severity', label: 'Severity', sortable: true },
    { key: 'message', label: 'Message', sortable: false },
    { key: 'findings', label: 'Findings', sortable: true },
  ];

  const findingsByRuleId = getFindingsByRuleIdMap(scan);

  const items = [...rules.values()].map((rule) => {
    return {
      id: rule.id,
      message: rule.message,
      severity: rule.severity,
      findings: findingsByRuleId.get(rule.id) ?? 0,
    };
  });

  return { primaryKey: fields[0].key, fields, items };
};
