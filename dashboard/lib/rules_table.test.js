import { allRulesTableDataProps } from './rules_table';
import { getTypicalScan, getRules } from '~/test_support/mock_scans';

describe('allRulesTableDataProps', () => {
  it('returns correct props for typical scan', () => {
    expect(allRulesTableDataProps(getTypicalScan(), getRules())).toEqual({
      primaryKey: 'id',
      fields: [
        { key: 'id', label: 'Rule ID', sortable: false },
        { key: 'severity', label: 'Severity', sortable: true },
        { key: 'message', label: 'Message', sortable: false },
        { key: 'findings', label: 'Findings', sortable: true },
      ],
      items: [
        {
          id: 'vcs.rules.bad-button',
          findings: 2,
          message: 'message for bad-button',
          severity: 'WARNING',
        },
        {
          id: 'vcs.rules.okay-button',
          findings: 1,
          message: 'message for okay-button',
          severity: 'INFO',
        },
        {
          id: 'vcs.rules.bad-alert',
          findings: 1,
          message: 'message for bad-alert',
          severity: 'WARNING',
        },
        {
          id: 'vcs.rules.okay-alert',
          findings: 0,
          message: 'message for okay-alert',
          severity: 'INFO',
        },
        {
          id: 'vcs.rules.bad-label',
          findings: 0,
          message: 'message for bad-label',
          severity: 'WARNING',
        },
        {
          id: 'vcs.rules.okay-label',
          findings: 0,
          message: 'message for okay-label',
          severity: 'INFO',
        },
      ],
    });
  });
});
