import { scanToTreeMapSeries, defaultOptions, findingsForNode } from './treemap';
import { enhanceScan } from './api';
import {
  invalidScans,
  getEmptyScan,
  getMinimalScan,
  getTypicalScan,
  makeResult,
} from '~/test_support/mock_scans';
import {
  groupByOptions,
  GROUP_BY_DIRECTORY,
  GROUP_BY_SEVERITY,
  GROUP_BY_FILE_TYPE,
  GROUP_BY_NOTHING,
} from '~/components/group_by.vue';

const groupBys = groupByOptions.map(({ value }) => value);

describe('scanToTreeMapSeries', () => {
  describe('common behaviours', () => {
    describe.each(groupBys)('given groupBy: %s', (groupBy) => {
      describe.each(invalidScans)('given invalid scan %p', (scan) => {
        it('throws', () => {
          expect(() => scanToTreeMapSeries(scan, { groupBy })).toThrow();
        });
      });

      describe('given an empty scan', () => {
        it('returns an empty data array', () => {
          expect(scanToTreeMapSeries(getEmptyScan(), { groupBy }).data).toEqual([]);
        });
      });

      describe('given valid scan', () => {
        it('sets defaults', () => {
          expect(scanToTreeMapSeries(getMinimalScan(), { groupBy })).toMatchObject(defaultOptions);
        });
      });
    });
  });

  describe.each`
    context        | groupBy               | name             | levels
    ${'no'}        | ${GROUP_BY_NOTHING}   | ${'Rules'}       | ${2}
    ${'severity'}  | ${GROUP_BY_DIRECTORY} | ${'Directories'} | ${3}
    ${'severity'}  | ${GROUP_BY_SEVERITY}  | ${'Severities'}  | ${3}
    ${'file type'} | ${GROUP_BY_FILE_TYPE} | ${'File types'}  | ${3}
  `('given $context grouping', ({ groupBy, levels, name }) => {
    it('sets the name and levels correctly', () => {
      const series = scanToTreeMapSeries(getMinimalScan(), { groupBy });

      expect(series.name).toBe(name);
      expect(series.levels.length).toBe(levels);
    });
  });

  it('no grouping returns the correct structure for a typical scan', () => {
    expect(scanToTreeMapSeries(getTypicalScan()).data).toMatchObject([
      {
        name: 'bad-button',
        value: 2,
      },
      {
        name: 'okay-button',
        value: 1,
      },
      {
        name: 'bad-alert',
        value: 1,
      },
    ]);
  });

  it('directory grouping returns the correct structure for a typical scan', () => {
    const results = [
      makeResult({ path: 'ee/spec/frontend/foo/bar_spec.js' }),
      makeResult({ path: 'spec/frontend/foo/bar_spec.js' }),
    ];

    expect(scanToTreeMapSeries({ results }, { groupBy: GROUP_BY_DIRECTORY }).data).toMatchObject([
      {
        name: 'ee/foo',
        children: [
          {
            name: 'bad-button',
            value: 1,
          },
        ],
      },
      {
        name: '~/foo',
        children: [
          {
            name: 'bad-button',
            value: 1,
          },
        ],
      },
    ]);
  });

  it('severity grouping returns the correct structure for a typical scan', () => {
    expect(
      scanToTreeMapSeries(getTypicalScan(), { groupBy: GROUP_BY_SEVERITY }).data,
    ).toMatchObject([
      {
        name: 'WARNING',
        children: [
          {
            name: 'bad-button',
            value: 2,
          },
          {
            name: 'bad-alert',
            value: 1,
          },
        ],
      },
      {
        name: 'INFO',
        children: [
          {
            name: 'okay-button',
            value: 1,
          },
        ],
      },
    ]);
  });

  it('file type grouping returns the correct structure for a typical scan', () => {
    expect(
      scanToTreeMapSeries(getTypicalScan(), { groupBy: GROUP_BY_FILE_TYPE }).data,
    ).toMatchObject([
      {
        name: 'vue',
        children: [
          {
            name: 'bad-button',
            value: 1,
          },
          {
            name: 'okay-button',
            value: 1,
          },
          {
            name: 'bad-alert',
            value: 1,
          },
        ],
      },
      {
        name: 'js',
        children: [
          {
            name: 'bad-button',
            value: 1,
          },
        ],
      },
    ]);
  });
});

describe('findingsForNode', () => {
  it('returns no findings given no scan', () => {
    expect(findingsForNode()).toEqual([]);
  });

  it('returns all findings given no node', () => {
    const scan = enhanceScan(getTypicalScan());

    expect(findingsForNode(scan)).toBe(scan.results);
  });

  it('returns the correct findings given a leaf node', () => {
    const scan = enhanceScan(getTypicalScan());
    const series = scanToTreeMapSeries(scan);
    const [node] = series.data;

    expect(node.name).toBe('bad-button');
    expect(findingsForNode(scan, node)).toEqual([scan.results[0], scan.results[1]]);
  });

  it('returns the correct findings given a group node', () => {
    const scan = enhanceScan(getTypicalScan());
    const series = scanToTreeMapSeries(scan, { groupBy: GROUP_BY_SEVERITY });
    const [node] = series.data;

    expect(node.name).toBe('WARNING');
    expect(findingsForNode(scan, node)).toEqual([
      scan.results[0],
      scan.results[1],
      // Note index 2 is missing, it's in another group
      scan.results[3],
    ]);
  });
});
