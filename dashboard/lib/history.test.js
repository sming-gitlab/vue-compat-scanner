import { historyToStackedColumnChartProps } from './history';
import { getHistory, getRules } from '~/test_support/mock_scans';

describe('historyToStackedColumnChartProps', () => {
  it('returns correct props for unfiltered history', () => {
    expect(historyToStackedColumnChartProps(getHistory(), getRules())).toEqual({
      groupBy: ['2021-01-01', '2022-02-02'],
      bars: [
        {
          name: 'bad-alert',
          data: [1, 2],
        },
        {
          name: 'bad-button',
          data: [1, 2],
        },
        {
          name: 'okay-button',
          data: [1, 1],
        },
        {
          name: 'okay-label',
          data: [0, 2],
        },
      ],
      xAxisType: 'category',
      xAxisTitle: 'Date',
      yAxisTitle: 'Findings',
    });
  });
});
