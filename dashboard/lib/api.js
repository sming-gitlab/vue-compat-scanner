import memoize from 'lodash/memoize';

const fetchJSON = async (path) => {
  const response = await fetch(path);

  if (!response.ok) {
    const error = new Error(`"${response.url}": ${response.statusText}`);
    error.response = response;
    throw error;
  }

  return response.json();
};

/**
 * Get the data structure describing existing scans.
 */
export const getScansSummary = memoize(() => fetchJSON('scans.json'));

/**
 * Get the data structure describing current rules.
 * @returns {Promise<Map<string, Rule>>} A map of rule ids to rule definitions.
 */
export const getRules = memoize(async () => {
  const rules = await fetchJSON('rules.json');
  return rules.reduce((acc, rule) => {
    acc.set(rule.id, rule);
    return acc;
  }, new Map());
});

/**
 * Decorate scan data with extra fields useful for display logic.
 */
export const enhanceScan = (scan) => {
  scan.results.forEach((finding, i) => {
    finding.id = i;
  });

  return scan;
};

/**
 * Returns the JSON output of a semgrep scan, with some modifications.
 *
 * Modifications:
 * - Each finding is given a unique `id`.
 */
export const getCurrentScan = async () => {
  const { current } = await getScansSummary();

  return enhanceScan(await fetchJSON(current.file));
};
