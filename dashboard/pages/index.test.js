/**
 * @jest-environment jsdom
 */

import { shallowMount } from '@vue/test-utils';
import GlSprintf from '@gitlab/ui/src/components/utilities/sprintf/sprintf.vue';
import App from '~/pages/index.vue';
import { getTypicalScan as mockTypicalScan } from '~/test_support/mock_scans';

jest.mock('~/lib/api', () => ({
  getCurrentScan: jest.fn().mockImplementation(() => Promise.resolve(mockTypicalScan())),
}));

it('renders a title', () => {
  const wrapper = shallowMount(App, {
    stubs: { GlSprintf },
  });

  expect(wrapper.text()).toContain('Findings as of');
});
