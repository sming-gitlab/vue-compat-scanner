# Component discovery, integration, and accessibility

We have many components in Pajamas that are fully functioning and ready to be integrated into the product. In order to ensure they are ready to be migrated, the following steps must occur:

## Discovery

1. Create or assign a `research` issue and attach it to the [related integration epic](https://gitlab.com/groups/gitlab-org/-/epics/3107). Ensure the issue has the `~"pajamas::identify"` label.
1. Identify all known variants of a particular component.
1. Create a semgrep rule in the Adoption Scanner for each variant.
1. Create a subepic for each variant that needs migrated under the corresponding `Integrate` epic. For example [`Pajamas component: Buttons - Integrate`](https://gitlab.com/groups/gitlab-org/-/epics/1049) includes a subepic for [`Buttons > Haml > Render form submit buttons with Pajamas::ButtonComponent`](https://gitlab.com/groups/gitlab-org/-/epics/8537).
1. Using the [batch issue creator](https://gitlab.com/gitlab-org/frontend/playground/batch-issue-creator), create issues for each instance of the variant that needs to be migrated.
1. Ensure a Pajamas-compliant version is available to migrate to.
   - If a Pajamas-compliant version is available, write migration guides in the description and label the issue(s) with `~"pajamas::integrate" ~"workflow::ready for development"`
   - If a Pajamas-compliant version is not available, open an issue in [GitLab UI](https://gitlab.com/gitlab-org/gitlab-ui) using the [Component issue template](https://gitlab.com/gitlab-org/gitlab-ui/-/blob/main/.gitlab/issue_templates/Component.md) and ping the Foundations PM for scheduling. If a ViewComponent is needed, open an issue in the main GitLab repo.

## Accessibility

Each component is reviewed against our accessibility standards in compliance with our [Accessibility Conformance Report](https://design.gitlab.com/accessibility/vpat). To perform an audit, follow the steps outlined below:

1. Create or assign an issue from the [Accessibility Audit epic](https://gitlab.com/groups/gitlab-org/-/epics/5387) and follow the steps outlined in the description to complete the audit.
1. Ensure issues are created and linked to the sub-epic for any improvements that need to be made to the audit.
1. Address blocking accessibility issues first. Blocking issues are issue that change the foundations of how the component is built and would thus require an additional migration effort to complete if changed.
1. Once all accessibility issues are addressed, perform the follow-up audit to ensure compliance.
1. If any updates are needed to the component within the GitLab project, ensure that integration issues are created.
