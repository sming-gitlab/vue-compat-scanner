it('works', () => {
  // ruleid: vm-access
  expect(wrapper.vm.foo).toBe('this');
  // ruleid: vm-access
  expect(w.vm.bar).toBe('this');
  // ruleid: vm-access
  expect(w.vm['qux']).toBe('this');

  // ruleid: vm-method-spy
  jest.spyOn(wrapper.vm, 'doFoo');
  // ruleid: vm-method-spy
  const submitSpy = jest.spyOn(wrapper.vm.$refs.banForm, 'submit');

  // ruleid: vm-assign
  wrapper.vm.foo = jest.fn();
  // ruleid: vm-assign
  dropdown.vm.$refs.searchInput.focus = jest.fn();
  // ruleid: vm-assign
  [, wrapper.vm.currentDate] = mockTimeframeQuarters[1].range;

  // ruleid: vtu-banned-method
  wrapper.setData({ foo: 'bar' });
  // ruleid: vtu-banned-method
  fooBar().setMethods({ foo() {} });

  // ok: vm-access
  wrapper.vm.$emit('foo');
});
