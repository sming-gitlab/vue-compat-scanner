#!/bin/bash

# shellcheck source=bin/common.sh
source "$(dirname "${BASH_SOURCE[0]}")"/common.sh

DEST="$ROOT_DIR/public"

if [ -n "$CI" ]; then
  mkdir -p "$DEST"
  cp -a "$ROOT_DIR"/tmp/dashboard/* "$DEST"
else
  # Don't abort if some files don't exist when run locally.
  set +e

  DEST="$ROOT_DIR/dashboard/static"
fi

cp "$ROOT_DIR/tmp/vcs-rules.yml" "$DEST"
cp -r "$ROOT_DIR/tmp/history" "$DEST"
cp "$ROOT_DIR"/tmp/*.json "$DEST"

if [ -n "$CI" ]; then
  find "$DEST" -type f -regex '.*\.\(htm\|html\|txt\|text\|js\|json\|css\|svg\)$' -not -path "$DEST/history/*" -exec brotli -f -k {} \;
  find "$DEST" -type f -regex '.*\.\(htm\|html\|txt\|text\|js\|json\|css\|svg\)$' -exec gzip -f -k {} \;
fi

warn "Done!"
