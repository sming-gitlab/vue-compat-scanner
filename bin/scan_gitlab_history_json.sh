#!/bin/bash

# shellcheck source=bin/common.sh
source "$(dirname "${BASH_SOURCE[0]}")"/common.sh

print_help() {
  cat <<HELP_MESSAGE
Usage: $(basename "$0")

Scans the GitLab repository at various points in history, and dumps the JSON \
results for each scanned commit under "$ROOT_DIR/tmp/history/".

To change how many historical commits are scanned, set the NUM_WEEKS_TO_SCAN \
environment variable.

$(common_help)
HELP_MESSAGE
}

for arg in "$@"; do
  if [ "$arg" = "-h" ] || [ "$arg" = "--help" ]; then
    print_help
    exit
  fi
done

# TODO: Find a better way of getting 6 months of history of the main branch.
# Using --shallow-since="6 months" doesn't work well due to long-lived feature
# branches.
#
# We may want to occasionally bump the tag here to keep the clone "small".
TAG_AT_LEAST_SIX_MONTHS_OLD="${CI_CLONE_EXCLUDE_FROM_TAG:-v14.8.0-ee}"
ensure_gitlab_repo --shallow-exclude="$TAG_AT_LEAST_SIX_MONTHS_OLD"

# Before this script terminates, attempt to restore the GitLab repository to
# the commit/ref it was checked out to before the script was run.
# Has no effect in CI.
trap attempt_to_restore_original_git_checkout EXIT

dates=$("$ROOT_DIR"/bin/get_historical_dates.py)
for date in $dates; do
  commit_hash=$(
    git log \
      --max-count=1 \
      --before="${date}T00:00:00+00:00" \
      --first-parent \
      --pretty=format:%H \
      "$MAIN_BRANCH"
  )

  warn "Scanning $date at $commit_hash..."

  "$ROOT_DIR"/bin/scan_gitlab.sh \
    "$commit_hash" \
    --json \
    --output "$ROOT_DIR/tmp/history/${date}_${commit_hash}.json"
done
