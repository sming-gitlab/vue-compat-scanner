const { rules } = require('eslint-config-airbnb-base/rules/style');

// Copied rules from airbnb, while allowing forIn and forOf
const restrictedAirBnbSyntax = rules['no-restricted-syntax'].filter((x) => {
  return x && x.selector && !['ForInStatement', 'ForOfStatement'].includes(x.selector);
});

module.exports = {
  ignorePatterns: ['rules/**'],
  root: true,
  env: {
    es2021: true,
    node: true,
  },
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
  },
  extends: ['airbnb-base', 'prettier'],
  rules: {
    // In node world, this seems to be the right config, packages don't need the js in the end
    'import/extensions': ['error', 'ignorePackages'],
    // Eh, who cares
    'import/prefer-default-export': 'off',
    // Semgrep values have camelCase
    camelcase: ['error', { ignoreDestructuring: true }],
    'no-underscore-dangle': [
      'error',
      {
        allow: [
          '__dirname', // We define this one ourselves, but this is a widely used variable in node
        ],
      },
    ],
    'no-restricted-syntax': ['error', ...restrictedAirBnbSyntax],
  },
};
